class Counter {
    constructor (){
        this.button = document.querySelector('component-button')
        this.label = document.querySelector('component-label')

        this.eventListeners()

        this.labelValue = 0
    }

    eventListeners(){
        document.addEventListener('substract', this.substract.bind(this))
        document.addEventListener('add', this.add.bind(this))

    }

    substract() {
        this.labelValue -= 1

        this.label.setAttribute('result', this.labelValue )
    }

    add() {
        this.labelValue += 1

        this.label.setAttribute('result', this.labelValue )
    }


}

new Counter()